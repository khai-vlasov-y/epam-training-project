FROM nginx:alpine
WORKDIR /usr/share/nginx/html
RUN apk --no-cache --update add curl
RUN echo 'Test nginx container! Continue development o_O!' > index.html
RUN echo 'nginx -t && nginx -g "daemon on;" && sleep 2 && curl -sSL localhost' > /test.sh && chmod 755 /test.sh
